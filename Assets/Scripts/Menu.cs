﻿using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
  public GameObject[] objectsToDisable;

  [SerializeField, HideInInspector]
  private Dropdown combo;
  [SerializeField, HideInInspector]
  private InputField editor;
  [SerializeField, HideInInspector]
  private Slider slider;
  [SerializeField, HideInInspector]
  private GameOfLife gameOfLife;

  public void ComboModified()
  {
    bool toEnable = combo.value == 0;
    for (int i = 0; i < objectsToDisable.Length; i++) objectsToDisable[i].SetActive(toEnable);
    gameOfLife.ResetGame((GameOfLife.Mode)combo.value);
  }

  public void EditorModified()
  {
    gameOfLife.size = int.Parse(editor.text);
    gameOfLife.ResetGame(gameOfLife.mode);
  }

  public void SliderModified()
  {
    gameOfLife.timeDelay = 1f/slider.value;
  }
}
