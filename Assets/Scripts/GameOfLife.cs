﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameOfLife : MonoBehaviour
{
  public int size = 100;
  public float timeDelay = 1f/40f;
  public RawImage myImage;
  public RawImage grid;
  public Text textFPS;
  public Texture2D[] patterns;
  public Mode mode;

  public enum Mode
  {
    Random,
    Manual,
    Glider,
    SmallSpaceship,
    MiddleSpaceship,
    BigSpaceship,
    Pulsar,
    GosperGun
  };

  private Texture2D[] textures = new Texture2D[2];
  private int step = 0;
  private readonly int[] neighborsX = new int[8] { -1, 0, 1, -1, 1, -1, 0, 1 };
  private readonly int[] neighborsY = new int[8] { -1, -1, -1, 0, 0, 1, 1, 1 };
  private IEnumerator game;
  private bool gamePaused = false;
  private int nbFPS = 0;
  private float timerFPS = 0;

  void Start()
  {
    // Random generation of content
    Init(Mode.Random);

    // Launch the game !
    game = LaunchGameOfLife();
    StartCoroutine(game);
  }

  private void Init(Mode newMode)
  {
    if (newMode == Mode.Random) // Random image
    {
      ResetImages();
      for (int i = 0; i < textures[0].width; i++)
      {
        for (int j = 0; j < textures[0].height; j++)
        {
          int value = Random.Range(0, 2);
          textures[0].SetPixel(i, j, value == 1 ? Color.white : Color.black);
        }
      }
      textures[0].Apply();
    }
    else if (newMode == Mode.Manual)
    {

    }
    else
    {
      gamePaused = true;
      Texture2D newTexture = patterns[(int)newMode - 2];
      for (int i = 0; i < 2; i++)
      {
        textures[i] = new Texture2D(newTexture.width, newTexture.height);
        textures[i].wrapMode = TextureWrapMode.Clamp;
        textures[i].filterMode = FilterMode.Point;
      }
      for (int i = 0; i < textures[0].width; i++)
      {
        for (int j = 0; j < textures[0].height; j++)
        {
          Color pixelColor = newTexture.GetPixel(i, j);
          textures[0].SetPixel(i, j, pixelColor);
        }
      }
    }

    mode = newMode;
    myImage.texture = textures[0];
  }

  public void ResetGame(Mode newMode)
  {
    step = 0;
    Init(newMode);
  }

  private void ResetImages()
  {
    for (int i = 0; i < 2; i++)
    {
      textures[i] = new Texture2D(size, size);
      textures[i].wrapMode = TextureWrapMode.Clamp;
      textures[i].filterMode = FilterMode.Point;
    }
    grid.gameObject.SetActive(size <= 200);
    if (size <= 200) grid.uvRect = new Rect(0, 0, size, size);
  }

  private IEnumerator LaunchGameOfLife()
  {
    timerFPS = Time.time;
    while (true)
    {
      yield return new WaitForSeconds(timeDelay);
      SetNewTextureValues();

      ++nbFPS;
      if (Time.time - timerFPS> 1f) DisplayFPS();
    }
  }

  private void SetNewTextureValues()
  {
    int nextStep = (step + 1) % 2;
    for (int i = 0; i < textures[step].width; i++)
    {
      for (int j = 0; j < textures[step].height; j++)
      {
        int nbValidneighbors = GetNbValidNeighbors(i, j);
        bool statusCurrentPixel = textures[step].GetPixel(i, j).r > 0.5f;
        if (statusCurrentPixel)
        {
          if (nbValidneighbors < 2 || nbValidneighbors > 3) textures[nextStep].SetPixel(i, j, Color.black);
          else textures[nextStep].SetPixel(i, j, Color.white);
        }
        else
        {
          if (nbValidneighbors == 3) textures[nextStep].SetPixel(i, j, Color.white);
          else textures[nextStep].SetPixel(i, j, Color.black);
        }
      }
    }
    textures[nextStep].Apply();
    myImage.texture = textures[nextStep];
    step = nextStep;
  }

  private int GetNbValidNeighbors(int x, int y)
  {
    int result = 0;
    for (int i = 0; i < neighborsX.Length; i++)
      if (HasValidNeighborAt(x + neighborsX[i], y + neighborsY[i]))
        result++;
    return result;
  }

  private bool HasValidNeighborAt(int x, int y)
  {
    if (x < 0) x += textures[0].width;
    else if (x >= textures[0].width) x -= textures[0].width;

    if (y < 0) y += textures[0].height;
    else if (y >= textures[0].height) y -= textures[0].height;

    return textures[step].GetPixel(x, y).r > 0.5f;
  }

  public void PausePlayGame(bool pause)
  {
    if (gamePaused == pause) return;

    if (pause) StopCoroutine(game);
    else StartCoroutine(game);

    gamePaused = pause;
  }

  private void DisplayFPS()
  {
    textFPS.text = nbFPS.ToString() + " FPS";
    timerFPS = Time.time;
    nbFPS = 0;
  }
}
